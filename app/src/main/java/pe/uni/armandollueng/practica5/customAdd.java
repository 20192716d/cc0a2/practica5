package pe.uni.armandollueng.practica5;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class customAdd extends AppCompatActivity {

   EditText editText;
   Button button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_custom_add);

        editText = findViewById(R.id.edit_text_custom);
        button = findViewById(R.id.boton_custom_add);

        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);

        int width = dm.widthPixels;
        int height = dm.heightPixels;

        getWindow().setLayout((int)(width*0.8),(int)(height*0.7));

        WindowManager.LayoutParams params = getWindow().getAttributes();
        params.gravity = Gravity.CENTER;
        params.x = 0;
        params.y = -20;

        getWindow().setAttributes(params);

        button.setOnClickListener(v -> {
            String valor = editText.getText().toString();
            if(!valor.equals("")){
                Intent intent = new Intent(getApplicationContext(), agregarCantidad.class);
                intent.putExtra("CANTIDAD","+ " + editText.getText().toString() + " ml");
                startActivity(intent);
                finish();
            }else{
                Toast.makeText(getApplicationContext(), R.string.msg_toast_custom,Toast.LENGTH_LONG).show();
            }

        });
    }
}