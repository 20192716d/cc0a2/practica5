package pe.uni.armandollueng.practica5;

public class Registro {
    String fecha;
    int cantidad;

    public Registro(String fecha, String cantidad) {
        this.fecha = fecha;
        this.cantidad = numeros(cantidad);
    }

    public static int numeros(String cantidad){
        char[] cadena = cantidad.toCharArray();
        StringBuilder n = new StringBuilder();
        for (char c : cadena) {
            if (Character.isDigit(c)) {
                n.append(c);
            }
        }
        return Integer.parseInt(n.toString());
    }

}
