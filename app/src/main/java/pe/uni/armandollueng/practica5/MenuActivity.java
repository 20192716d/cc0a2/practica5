package pe.uni.armandollueng.practica5;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.regex.Pattern;

public class MenuActivity extends AppCompatActivity {

    GridView gridView;
    ArrayList<String> text = new ArrayList<>();
    ArrayList<Integer> image = new ArrayList<>();
    ArrayList<Registro> registros = new ArrayList<>();
    LinearLayout linearLayout;
    TextView textViewCantidad;
    TextView text_view_registro;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);

        gridView = findViewById(R.id.grid_view);
        linearLayout = findViewById(R.id.linear_layout);
        textViewCantidad = findViewById(R.id.text_view_cantidad);
        text_view_registro = findViewById(R.id.registro);

        try {
            fillArray();
            leerRegistro();
        } catch (NoSuchFieldException | FileNotFoundException e) {
            e.printStackTrace();
        }

        GridAdapter gridAdapter = new GridAdapter(this, text, image);
        gridView.setAdapter(gridAdapter);

        gridView.setOnItemClickListener((parent, view, position, id) -> {
            Intent intent;
            if (position != 3) {
                intent = new Intent(MenuActivity.this, agregarCantidad.class);
                intent.putExtra("CANTIDAD", text.get(position));
            }else {
                intent = new Intent(getApplicationContext(), customAdd.class);
            }
            startActivity(intent);
        });
    }

    private void fillArray() throws NoSuchFieldException {
        text.add("+ 100 ml");
        text.add("+ 200 ml");
        text.add("+ 500 ml");
        text.add("Custom");

        image.add(R.raw.cien);
        image.add(R.raw.doscientos);
        image.add(R.raw.quinientos);
        image.add(R.raw.cien);
    }

    private void leerRegistro() throws FileNotFoundException {
        try {
            String FILE_NAME = "registros.txt";
            File tarjetaSD = Environment.getExternalStorageDirectory();
            File ruta = new File(tarjetaSD.getPath(), FILE_NAME);
            InputStreamReader abrirArchivo =  new InputStreamReader(openFileInput(FILE_NAME));
            BufferedReader leerArchivo =  new BufferedReader(abrirArchivo);
            String linea = leerArchivo.readLine();
            while(linea != null){
                String separador = Pattern.quote("|");
                String[] separaciones = linea.split(separador);
                String fecha = separaciones[0];
                String cantidad = separaciones[1];
                registros.add(new Registro(fecha, cantidad));
                linea = leerArchivo.readLine();
            }

            leerArchivo.close();
            abrirArchivo.close();

            int cantidad = 0;
            LocalDate todaysDate = LocalDate.now();
            String fecha = todaysDate.toString();
            StringBuilder registro_del_dia= new StringBuilder();
            for(Registro registro:registros){
                if(fecha.equals(registro.fecha)){
                    registro_del_dia.append(registro.fecha).append(": ").append(registro.cantidad).append(" ml\n");
                    cantidad += registro.cantidad;
                }
            }
            String cantidadS = cantidad + " ml";
            textViewCantidad.setText(cantidadS);
            text_view_registro.setText(registro_del_dia);

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

}