package pe.uni.armandollueng.practica5;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.time.LocalDate;

public class agregarCantidad extends AppCompatActivity {

    TextView textView;
    Button buttonRegresarMenu;
    String cantidadS;
    String FILE_NAME = "registros.txt";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_agregar_cantidad);

        textView = findViewById(R.id.text_view_agregar);
        buttonRegresarMenu = findViewById(R.id.boton_regresar_menu);

        Intent intent = getIntent();
        cantidadS = intent.getStringExtra("CANTIDAD");
        textView.setText(cantidadS);

        buttonRegresarMenu.setOnClickListener(v -> {
            try {
                saveFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
            Intent intent1 =  new Intent(agregarCantidad.this, MenuActivity.class);
            startActivity(intent1);
        });

    }

    private void saveFile() throws IOException {
        LocalDate todaysDate = LocalDate.now();
        String fecha = todaysDate.toString();
        StringBuilder textoCom = new StringBuilder();

        InputStreamReader abrirArchivo =  new InputStreamReader(openFileInput(FILE_NAME));
        BufferedReader leerArchivo =  new BufferedReader(abrirArchivo);
        String linea = leerArchivo.readLine();
        while(linea != null){
            textoCom.append(linea).append("\n");
            linea = leerArchivo.readLine();
        }
        leerArchivo.close();
        abrirArchivo.close();

        String texto_a_guardar = textoCom + fecha + "|" + cantidadS + "\n";
        File tarjetaSD = Environment.getExternalStorageDirectory();
        File ruta = new File(tarjetaSD.getPath(), FILE_NAME);
        OutputStreamWriter createArchivo =
                new OutputStreamWriter(openFileOutput(FILE_NAME, Activity.MODE_PRIVATE));
        createArchivo.write(texto_a_guardar);
        createArchivo.flush();
        createArchivo.close();
        Toast.makeText(this,"Guardado Correctamente en " + ruta.getPath(), Toast.LENGTH_SHORT).show();
    }
}